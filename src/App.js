import React from 'react';
import './App.css';
import Login from './Login/login';
import { Route, BrowserRouter, Switch } from 'react-router-dom';

function App() {
  return (

    <BrowserRouter>
      <div className="App">
        <Switch>
          <Route path="/" component={Login} />
          <Route path="/login" component={Login} />
        </Switch>
      </div>
        <div>
            Hello World?
        </div>
    </BrowserRouter>
  );
}

export default App;
