import React from 'react';
import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { css } from "glamor";

class Login extends React.Component {
    state = {
        email: '',
        password: '',
    }

    toastMsg = (message, c) => {
        toast(message, {
            className: css({
                background: c + ' !important',
                color: 'white',
                fontWeight: 'bold'
            }),
            hideProgressBar: true
        });
    }

    submitHandle = (event) => {
        event.preventDefault();

        axios.post(
            'https://5r1clg9mvf.execute-api.us-east-2.amazonaws.com/dev/users',
            {
                'email': this.state.email,
                'password': this.state.password
            },
            {
                headers:
                {
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*'
                }
            }
        ).then(res => {
            console.log(res)
            let result = res.data['success']
            this.toastMsg(result, 'green')
        })
            .catch(error => {
                console.log(error, 'red');
                this.toastMsg('login failure', 'red')
            })
    }

    onChangeHandle = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };



    render() {
        return (
            <div>
                <h1>Login page</h1>
                <form onSubmit={this.submitHandle}>
                    <label htmlFor="email">Email</label>
                    <input name="email" type="email" placeholder="Email" id={"email"} value={this.state.email} onChange={this.onChangeHandle} />
                    <label htmlFor="email">Password</label>
                    <input name="password" type="password" placeholder="Password" id={"password"} value={this.state.password} onChange={this.onChangeHandle} />
                    <button type="submit" id={"loginButton"}>Login</button>
                </form>

                <ToastContainer />
            </div>
        )
    }
}

export default Login;