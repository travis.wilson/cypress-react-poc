context('Actions', () => {
    beforeEach(() => {
        // cy.visit('http://localhost:3000')
        cy.visit('https://master.d2art2teq3gvku.amplifyapp.com/')
    });
    // describe('The Login Page', function () {
    //     it('successfully loads', function () {
    //         cy.visit('http://localhost:3000') // change URL to match your dev URL
    //     })
    // });

    describe('My First Test', function () {
        it('Visits Fake “Hello Login”!', function () {
            cy.contains('Login').click()
            // Get an input, type into it and verify that the value has been updated
            cy.get('#email')
                .type('fake@email.com')
                .should('have.value', 'fake@email.com')
        })
    })
});